// ==UserScript==
// @name Digital Operations Salesforce Comments Tool
// @namespace  www.cobaltgroup.com
// @include https://cdk.my.salesforce.com/00a*
// @version 1
// @author       Joe Childress
// @grant        none
// ==/UserScript==
// Document ready helper

(function () {
	function getScript(source, callback) {
		var script = document.createElement('script');
		script.async = 1;
		script.onload = script.onreadystatechange = function (_, isAbort) {
			if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
				script.onload = script.onreadystatechange = null;
				script = undefined;
				if (!isAbort) {
					if (callback) callback();
				}
			}
		};
		script.src = source;
		document.querySelector("head").appendChild(script);

	}

	//CUSTOM COLOR AND COMMENTS
	window.contentCommentOptions = {
		customAccentColor: '', //OPTIONAL CUSTOM COLOR. HEX, RGB, RGBA or Name. USES DEFAULT COLOR IF EMPTY ''
		customActive: true, //SET TO TRUE IF YOU WANT TO HAVE CUSTOM COMMENTS ADDED
		customComments: [ //ADD CUSTOM COMMENTS TO THIS USING THE SAME COMMA SEPARATED OBJECTS
			{
				title: "3rd Party- Unique/Jira",
				comment: "A unique install has been added to the site and the case has been added to the Partner Program ticket for the official integration record when completed.\n\nWaiting for the Partner Program."
            },
			{
				title: "3rd Party- Unable To Install/Jira",
				comment: "An integration record does not exist for this code. The case has been added to the Partner Program ticket for the official integration record when completed.\n\nWaiting for the Partner Program."
            }
        ]
	}

	//Spreadsheet Version
	//getScript("https://media-dmg.assets-cdk.com/teams/repository/export/599/12e60dcd610058bf00050568b5709/59912e60dcd610058bf00050568b5709.js");

	//Comment JS File Version
	//getScript("https://media-dmg.assets-cdk.com/teams/repository/export/f75/15220dc85100582210050568b5709/f7515220dc85100582210050568b5709.js");

	// })();


function whenDocumentIsReady(fn) {
		if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
			fn();
		} else {
			document.addEventListener('DOMContentLoaded', fn);
		}
	}

	whenDocumentIsReady(function () {

		const cdkCommentTool = false;
		const commentArea = document.querySelector('#CommentBody');

		//ADD FLEXBOX TO CONTAINER
		document.querySelectorAll('.requiredInput')[1].style.cssText = "display: flex; align-items: top";

		//ADD LOADING ANIMATION
		//COLOR OPTIONS
		const defaultAccentColor = '#1797c0'; //SF BLUE
		const accentColor = (contentCommentOptions.customAccentColor ? contentCommentOptions.customAccentColor : defaultAccentColor);

		//WRAPPER
		const commentLoadWrapper = document.createElement('div');
		commentLoadWrapper.style.cssText = 'display: flex;align-items: center; justify-content: center; width: 300px; margin-left: 30px;color:' + accentColor + ';';
		commentLoadWrapper.setAttribute('id', 'content-comments-wrapper');

		//LOADER IMAGE
		const commentLoadImg = document.createElement('img');

		//circle bars

		commentLoadImg.src = "https://media-dmg.assets-cdk.com/teams/repository/export/5d9/0cc08f50b100584a70050568ba825/5d90cc08f50b100584a70050568ba825.gif"
		commentLoadImg.setAttribute('id', 'content-comments-loadImg');
		commentLoadImg.style.cssText = 'height: 80px;';


		//APPEND DIV AND IMG
		commentLoadWrapper.appendChild(commentLoadImg);
		commentArea.parentNode.insertBefore(commentLoadWrapper, commentArea.nextSibling)

		//SPREADSHEET
		const script_url = "https://script.google.com/macros/s/AKfycbyH66VHwghm7BkZvM3R92l0aFIz-N0h2055R-OVeSRTZoVKV5w/exec";

		// Set up our HTTP request
		const xhr = new XMLHttpRequest();

		// Setup our listener to process completed requests
		xhr.onload = function () {

			// Process our return data
			if (xhr.status >= 200 && xhr.status < 300) {
				const commData = JSON.parse(xhr.response);
				addComData(commData);
			} else {
				// What do when the request fails
				console.log('The Comment Tool request failed!');
				noToolMessage();
			}

		};

		const commUrl = script_url + "?action=read";

		xhr.open('GET', commUrl);
		xhr.send();

		function addComData(json) {
			const contentComments = json.records;

			if (commentArea && contentComments) {

				//COLOR OPTIONS
				const defaultAccentColor = '#1797c0'; //SF BLUE

				//TRIM OPTIONAL COLOR
				if (contentCommentOptions.customAccentColor) {
					contentCommentOptions.customAccentColor = contentCommentOptions.customAccentColor.trim();
				}

				const accentColor = (contentCommentOptions.customAccentColor ? contentCommentOptions.customAccentColor : defaultAccentColor);

				//BUILD SELECTOR
				//WRAPPER
				const commentSelectorWrapper = document.createElement('div');
				commentSelectorWrapper.style.cssText = 'display: inline-block; margin-left: 30px;color:' + accentColor + ';';
				commentSelectorWrapper.setAttribute('id', 'content-comments-wrapper');

				//LABEL FOR SELECTOR
				const commentSelectorLabel = document.createElement('label');
				commentSelectorLabel.setAttribute('for', 'content-comments');
				commentSelectorLabel.style.cssText = 'display:block; padding-bottom: 5px; font-weight: bold; font-size: 16px; padding-bottom: 16px;';
				addTextNode(commentSelectorLabel, 'Digital Operations Comments');

				//SELECTOR
				const commentSelector = document.createElement('select');
				commentSelector.setAttribute('id', 'content-comments');
				commentSelector.setAttribute('size', '8');
				commentSelector.style.cssText = 'margin-bottom:20px;border-color:' + accentColor + ';color:' + accentColor + ';';

				//ADD OPTIONS FROM STANDARD AR
				if (contentComments) {
					for (let i = 0; i < contentComments.length; i++) {
						if (contentComments[i].comment !== '' && contentComments[i].title !== '') {
							const option = document.createElement('option');
							option.value = contentComments[i].comment;
							option.text = contentComments[i].title;
							commentSelector.appendChild(option);
						}
					}
				}

				if (contentCommentOptions.customActive && contentCommentOptions.customComments.length > 0) {

					//CUSTOM SECTION LABEL
					const optionCustLine = document.createElement('option');
					optionCustLine.disabled = true;
					optionCustLine.text = '------ Custom ------';
					commentSelector.appendChild(optionCustLine);

					for (let i = 0; i < contentCommentOptions.customComments.length; i++) {
						const option = document.createElement('option');
						option.value = contentCommentOptions.customComments[i].comment;
						option.text = contentCommentOptions.customComments[i].title;
						commentSelector.appendChild(option);
					}
				}

				//BUTTON
				const commentBtn = document.createElement('input');
				commentBtn.setAttribute('class', 'btn');
				commentBtn.setAttribute('value', 'Add');
				commentBtn.style.cssText = 'display: block; width: 50px; text-align: center;border:none; padding-top:5px; padding-bottom:5px; color:white; background:' + accentColor + ';';

				//BUTTON FN
				commentBtn.addEventListener('click', function () {
					commentArea.value = commentSelector.options[commentSelector.selectedIndex].value;
				});

				//APPEND ELEMENTS
				commentSelectorWrapper.appendChild(commentSelectorLabel);
				commentSelectorWrapper.appendChild(commentSelector);
				commentSelectorWrapper.appendChild(commentBtn);

				//REMOVE LOAD
				commentLoadWrapper.parentNode.removeChild(commentLoadWrapper);

				//ADD COMMENT TOOL
				commentArea.parentNode.insertBefore(commentSelectorWrapper, commentArea.nextSibling);

				cdkCommentTool = true;
			}

		}

		//HELPER FN TO APPND TEXT TO EL
		function addTextNode(el, text) {
			const newtext = document.createTextNode(text);
			el.appendChild(newtext);
		}

		//ADD ERROR MESSAGE IF NOT LOADED
		function noToolMessage() {
			if (!cdkCommentTool) {
				const noTooldiv = document.createElement('div');
				noTooldiv.setAttribute('id', 'noTooldiv');
				noTooldiv.innerHTML = "The Comment Tool is not currently available.<br>Refer to your team comment documenation for approved messages.";
				commentLoadImg.parentNode.removeChild(commentLoadImg);
				commentLoadWrapper.appendChild(noTooldiv);
			}
		}

		//LOAD CHECK AND ADD MESSAGE IF NOT LOADED
		setTimeout(noToolMessage, 8000)

	}); //END WHEN DOCREADY FN



})(); //END OF MAIN INSTALL SCRIPT